// Model
// const model = tf.model({ inputs: input, outputs: output });

let model = getModel()

let xs = tf.randomUniform([2,224,224,1]);

const ys = tf.randomUniform([2,2])
ys.print()
train().then(() => {
    console.log('training complete')
    let outputs = model.predict(xs)
    outputs.print()
    console.log(model.summary())

  })
  
  async function train() {
    for(let i= 0; i<10; i++) {
        const response = await model.fit(xs, ys, {
          // batchSize: BATCH_SIZE,
          // validationData: [testXs, testYs],
          // epochs: 10,
          shuffle: true
        });
        console.log(response.history.loss[0])
    }
  }

function getModel() {

    const model = tf.sequential()

    const IMAGE_WIDTH = 224;
    const IMAGE_HEIGHT = 224;
    const IMAGE_CHANNELS = 1; 

    // In the first layer of our convolutional neural network we have 
    // to specify the input shape. Then we specify some parameters for 
    // the convolution operation that takes place in this layer.
    model.add(tf.layers.conv2d({
        inputShape: [IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS],
        kernelSize: 3,
        filters: 3,
        activation: 'relu',
        kernelInitializer: 'varianceScaling'
      }));
    
      
      // Repeat another conv2d + maxPooling stack. 
      // Note that we have more filters in the convolution.
      model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 32,
        padding: "same",
      }));
      
      // Now we flatten the output from the 2D filters into a 1D vector to prepare
      // it for input into our last layer. This is common practice when feeding
      // higher dimensional data to a final classification output layer.
      model.add(tf.layers.flatten());
    
      // Our last layer is a dense layer which has 10 output units, one for each
      // output class (i.e. 0, 1, 2, 3, 4, 5, 6, 7, 8, 9).
      const NUM_OUTPUT_CLASSES = 2;
      model.add(tf.layers.dense({
        units: NUM_OUTPUT_CLASSES,
        // kernelInitializer: 'varianceScaling',
        activation: 'softmax'
      }));
    
      
      // Choose an optimizer, loss function and accuracy metric,
      // then compile and return the model
      // const optimizer = tf.train.adam();
    const optimizer = tf.train.sgd(0.3)

      model.compile({
        optimizer: optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy'],
      });

      return model
}