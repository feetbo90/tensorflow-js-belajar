let r, g, b;
let database;
function pickColor() {
    r = floor(random(256))
    g = floor(random(256))
    b = floor(random(256))
    background(r,g,b);

}

function setup() {

    var firebaseConfig = {
        apiKey: "AIzaSyDrZfKlZ3Qh7CTHPL49d0WLzNcNg6XhTUQ",
        authDomain: "color-classification-43406.firebaseapp.com",
        projectId: "color-classification-43406",
        storageBucket: "color-classification-43406.appspot.com",
        messagingSenderId: "747782938941",
        appId: "1:747782938941:web:61f050dee606c60453a554",
        measurementId: "G-TJHE1SBH97"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
    // console.log(firebase)
    database = firebase.database();

    createCanvas(100, 100);
    pickColor();

    

    // let radio = createRadio();
    // radio.option('red-ish')
    // radio.option('green-ish')
    // radio.option('blue-ish')
    let buttons = []
    buttons.push(createButton('red-ish'))
    buttons.push(createButton('green-ish'))
    buttons.push(createButton('blue-ish'))
    buttons.push(createButton('orange-ish'))
    buttons.push(createButton('yellow-ish'))
    buttons.push(createButton('pink-ish'))
    buttons.push(createButton('purple-ish'))
    buttons.push(createButton('brown-ish'))
    buttons.push(createButton('grey-ish'))

    for (let i = 0; i < buttons.length; i++)
    {
        buttons[i].mousePressed(sendData)
    }
   
    // let submit = createButton('submit');
    // submit.mousePressed(sendData);

    
}

function sendData() {
    console.log(this.html())
    let colorDatabase = database.ref('colors')

    var data = {
        r: r,
        g: g,
        b: b,
        label: this.html()
    }

    console.log('saving data ...')
    console.log(data)

    let color = colorDatabase.push(data, finished)


    function finished(err) {
        if (err) {
            console.log("oops, something wrong")
            console.log(err)
        } else {
            console.log("Data saved successfully")
            pickColor()
        }
    }
}