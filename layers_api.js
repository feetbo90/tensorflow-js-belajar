const model = tf.sequential();

/*
    input layer terdiri 2 unit
    hidden layer terdiri 4 units
    output layer terdiri 3 units
*/

const configHidden = {
    units: 4,
    inputShape: [2],
    activation: 'sigmoid'
}

const hidden = tf.layers.dense(configHidden);

const configOutput = {
    units: 1,
    // inputShape: [4],
    activation: 'sigmoid'
}

const output = tf.layers.dense(configOutput);

model.add(hidden)
model.add(output)

const sgdOpt = tf.train.sgd(0.7)
const config = {
    optimizer: sgdOpt,
    loss: tf.losses.meanSquaredError
}

model.compile(config)
const xs = tf.tensor2d([
    [0, 0],
    [0.5, 0.5],
    [1, 1],
    // [0.35,0.82],
]);
const ys = tf.tensor2d([
    [1],
    [0.5],
    [0],
    // [0.1, 0.2, 0.02],
])

// console.log(tf.shape(xs))
// let();
const configTwo = {
    // verbose: true,
    shuffle: true,
    epoch: 10
}

// model.fit(xs, ys, configTwo).then((response) => console.log(response.history.loss[0]))
train().then(() => {
    console.log('training complete')
    let outputs = model.predict(xs)
    outputs.print()
})

async function train() {
    for(let i= 0; i<1000; i++) {
        const response = await model.fit(xs, ys, configTwo)
        console.log(response.history.loss[0])
    }
}
console.log(model.summary())




