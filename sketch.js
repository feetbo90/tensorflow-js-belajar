function setup() {
    

} 

function draw() {
   
    // c.mul(d).print();

    // funsgi dispose untuk menghapus memory pemakaian variabel
    
    // a.dispose();
    // b.dispose();
    // c.dispose();
    // d.dispose();
    // data.dispose();
    // vtense.dispose();

    tf.tidy(myStuff);
    console.log("ini memory : ")
    console.log(tf.memory().numTensors);
    // console.log(b.print())
    // a.matMul(b).print();  // or tf.matMul(a, b)
    // tf.dispose(a);
    // tf.dispose(b);
}

function myStuff() {
    const data = tf.tensor([1, 2, 3, 4, 5, 6, 7, 8], [2, 2, 2])
    // tensor tidak dapat di rubah datanya
    // sehingga dibutuhkan variable untuk mengubah tf.variable
    data.print()
    const vtense = tf.variable(data)
    console.log(vtense)

    // matriks multiplication
    
    const a = tf.tensor2d([1, 2], [1, 2]);
    const c = tf.tensor([3, 0, 1, 1, 5, 8, 2, 7, 2], [3, 3])
    const d = tf.tensor([1, 0, -1, 1, 0, -1, 1, 0, -1], [3, 3])
    const b = tf.tensor2d([1, 2, 3, 4], [2, 2]);

    console.log(c.print())
    console.log(d.print())
    console.log("ini matmul : ")
}